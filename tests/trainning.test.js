const dbHandler = require('./db-handler')
const Trainings = require('../models/Training')

beforeAll(async () => {
    await dbHandler.connect()
})

afterEach(async () => {
    await dbHandler.clearDB()
});

afterAll(async () => {
    await dbHandler.closeDB()
});

const TrainingCompleted = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingNameNull = {
    title: "",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingNameOneCharacter = {
    title: "t",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingDateNull = {
    title: "abc",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: " ",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingStartTimeNull = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingStartTimeIncorrectFormat = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingEndTimeNull = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingEndTimeIncorrectFormat = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingMaxParticipantsBeZero = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 0,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingMaxParticipantsNegativeNumber = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: -5,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingMaxParticipantsMorethanMaxLength = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 500,
    hours: 2,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingHoursBeZero = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 0,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingHoursNegativeNumber = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: -1,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}
const TrainingHoursMoreThanMaxLength = {
    title: "เตรียมความพร้อม (อบรมวิชาการ)",
    company: "5e67c92e0fc2df7cdc2034f9",
    type: "เตรียมความพร้อมสหกิจศึกษา",
    startDate: "2020-04-10T00:00:00.000+00:00",
    startTime: "18:00",
    endTime: "19:00",
    location: "5e7e05d7a22fef4504de5ec6",
    maxParticipants: 5,
    hours: 50,
    participants: [
        { student: "5e8ee0b89237860ee42582aa", status: true },
        { student: "5e9036de1c2e5edcec790583", status: true }
    ],
    isPublished: true,
    isChecked: true
}

describe('Training Completed', () => {
    it('เพิ่มการอบรมใหม่ได้', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingCompleted)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).toBeNull()
    });
});

describe('Training Name', () => {
    it('เพิ่มรายการอบรมใหม่ไม่ได้ หัวข้อ เป็นค่าว่าง', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingNameNull)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
    it('เพิ่มรายการอบรมใหม่ไม่ได้ หัวข้อ น้อยกว่า 1 ตัวอักษร', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingNameOneCharacter)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });

});

describe('Training Date', () => {
    it('เพิ่มรายการอบรมใหม่ไม่ได้ วันที่อบรม เป็นค่าว่าง', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingDateNull)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });

});

describe('Training Start Time', () => {
    it('เพิ่มรายการอบรมใหม่ไม่ได้ เวลาที่เริ่ม เป็นค่าว่าง', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingStartTimeNull)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
    it('เพิ่มรายการอบรมใหม่ไม่ได้ เวลาที่เริ่ม ไม่ถูกรูปแบบ', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingStartTimeIncorrectFormat)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
});

describe('Training End Time', () => {
    it('เพิ่มรายการอบรมใหม่ไม่ได้ เวลาสิ้นสุด เป็นค่าว่าง', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingEndTimeNull)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
    it('เพิ่มรายการอบรมใหม่ไม่ได้ เวลาที่เริ่ม ไม่ถูกรูปแบบ', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingEndTimeIncorrectFormat)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
});

describe('Training MaxParticipants', () => {
    it('เพิ่มรายการอบรมใหม่ไม่ได้ จำนวนคนที่รับ ต้องไม่เป็น 0', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingMaxParticipantsBeZero)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
    it('เพิ่มรายการอบรมใหม่ไม่ได้ จำนวนคนที่รับ ต้องไม่ติดลบ', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingMaxParticipantsNegativeNumber)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
    it('เพิ่มรายการอบรมใหม่ไม่ได้ จำนวนคนที่รับ ต้องไม่เกิน 300 คน', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingMaxParticipantsMorethanMaxLength)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
});

describe('Training Hours', () => {
    it('เพิ่มรายการอบรมใหม่ไม่ได้ จำนวนชั่วโมงที่ได้รับ ต้องไม่เป็น 0', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingHoursBeZero)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
    it('เพิ่มรายการอบรมใหม่ไม่ได้ จำนวนชั่วโมงที่ได้รับ ต้องไม่ติดลบ', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingHoursNegativeNumber)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
    it('เพิ่มรายการอบรมใหม่ไม่ได้ จำนวนชั่วโมงที่ได้รับ ต้องไม่เกิน 24 ชั่วโมง', async () => {
        let res = null
        try {
            const training = new Trainings(TrainingHoursMoreThanMaxLength)
            await training.save()
        } catch (err) {
            res = err
        }
        expect(res).not.toBeNull()
    });
});