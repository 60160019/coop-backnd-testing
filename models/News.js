const mongoose = require('mongoose')
const Schema = mongoose.Schema

const newsSchema = new Schema({
  title: String,
  content: String,
  publishedDate: Date,
  isPublished: Boolean
})

module.exports = mongoose.model('News', newsSchema, 'News')
