const mongoose = require('mongoose')
const Schema = mongoose.Schema

const companySchema = new Schema({
  name: String,
  contact: String,
  tel: String,
  email: String,
  address: String
})

module.exports = mongoose.model('Companies', companySchema, 'Companies')
