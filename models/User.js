const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
  username: String,
  password: String,
  type: Number,
  name: String,
  surname: String,
  tel: String,
  email: String
})

module.exports = mongoose.model('Users', userSchema, 'Users')