const mongoose = require('mongoose')
const Schema = mongoose.Schema

const studentSchema = new Schema({
  username: String,
  password: String,
  type: Number,
  name: String,
  surname: String,
  branch: String,
  grade: Number,
  credit: Number,
  gpax: Number,
  subjects: [{}],
  ability: {},
  teacher: { type: Schema.Types.ObjectId, ref: 'Teacher' }
})

module.exports = mongoose.model('Students', studentSchema, 'Students')
