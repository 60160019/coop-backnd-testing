const mongoose = require('mongoose')
const Schema = mongoose.Schema

const coopSchema = new Schema({
  title: String,
  company: { type: Schema.Types.ObjectId, ref: 'Companies' },
  position: { type: Schema.Types.ObjectId, ref: 'JobPositions' },
  maxApproved: Number,
  closingDate: Date,
  detail: String,
  interview: {
    date: Date,
    startTime: String,
    endTime: String,
    location: { type: Schema.Types.ObjectId, ref: 'Locations' }
  },
  registers: [
    {
      student: { type: Schema.Types.ObjectId, ref: 'Students' },
      status: Number
    } // status 0 = รอคัดเลือก, 1 = รอสัมภาษณ์, 2 = รอผลสัมภาษณ์, 3 = ผ่าน , 4 = ไม่ผ่าน
  ],
  isPublished: Boolean,
  isChecked: Boolean
})

module.exports = mongoose.model('Coops', coopSchema, 'Coops')
