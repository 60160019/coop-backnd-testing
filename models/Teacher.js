const mongoose = require('mongoose')
const Schema = mongoose.Schema

const teacherSchema = new Schema({
  namePrefix: String,
  name: String,
  surname: String,
  tel: String,
  email: String
})

module.exports = mongoose.model('Teachers', teacherSchema, 'Teachers')
