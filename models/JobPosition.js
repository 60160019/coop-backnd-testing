const mongoose = require('mongoose')
const Schema = mongoose.Schema

const jobPositionSchema = new Schema({
  name: String
})

module.exports = mongoose.model('JobPositions', jobPositionSchema, 'JobPositions')