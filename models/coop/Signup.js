const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SignuplistsltSchema = new Schema({
  title: String,
  company: String,
  amount: Number,
  closedate: Date
})

module.exports = mongoose.model(
  'Signuplists',
  SignuplistsltSchema,
  'Signuplists'
)
