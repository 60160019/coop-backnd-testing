const mongoose = require('mongoose')
const Schema = mongoose.Schema

const trainingSchema = new Schema({
  title: { type: String, required: true, minlength: 2 },
  company: { type: Schema.Types.ObjectId, ref: 'Companies' },
  type: { String, enum: ["เตรียมความพร้อมสหกิจศึกษา", "ทักษะทางวิชาการ"] },
  startDate: { type: Date, required: true, min: new Date(new Date().setHours(0, 0, 0, 0)) },
  startTime: { type: String, required: true, minlength: 5 },
  endTime: { type: String, required: true ,minlength: 5 },
  location: { type: Schema.Types.ObjectId, ref: 'Locations' },
  maxParticipants: { type: Number, required: true, min: 1, max: 300 },
  hours: { type: Number, required: true, min: 1, max: 24 },
  participants: [
    { student: { type: Schema.Types.ObjectId, ref: 'Students' }, status: Boolean }
  ],
  isPublished: Boolean,
  isChecked: Boolean
})

module.exports = mongoose.model('Trainings', trainingSchema, 'Trainings')
