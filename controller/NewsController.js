const News = require('../models/News')
const newsController = {
  async getManyNews (req, res, next) {
    let news
    try {
      if (req.params.isPublished === 'isPublished') {
        news = await News.find({ isPublished: true }).sort({
          publishedDate: -1
        })
      } else {
        news = await News.find({}).sort({
          _id: -1
        })
      }

      res.json(news)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getNews (req, res, next) {
    const { id } = req.params
    try {
      const news = await News.findById(id)
      res.json(news)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addNews (req, res, next) {
    const news = new News(req.body)
    try {
      await news.save()
      res.json(news)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateNews (req, res, next) {
    try {
      const news = await News.updateOne({ _id: req.body._id }, req.body)
      res.json(news)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteNews (req, res, next) {
    const { id } = req.params
    try {
      const news = await News.deleteOne({ _id: id })
      res.json(news)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = newsController
