const Coop = require('../models/Coop')
const coopController = {
  async getCoops (req, res, next) {
    try {
      let coops
      if (req.params.isPublished === 'isPublished') {
        coops = await Coop.find({
          isPublished: true,
          closingDate: { $gte: new Date() }
        })
          .populate('company')
          .populate('position')
          .populate('interview.location')
          .populate('register.student')
          .sort({ _id: -1 })
      } else {
        coops = await Coop.find({})
          .populate('company')
          .populate('position')
          .populate('interview.location')
          .populate('register.student')
          .sort({ _id: -1 })
      }

      res.json(coops)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getClosedCoops (req, res, next) {
    try {
      const coops = await Coop.find({ closingDate: { $lt: new Date() } })
        .populate('company')
        .populate('position')
        .populate('interview.location')
        .populate('register.student')
        .sort({ _id: -1 })
      res.json(coops)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async isRegisteredAnother (req, res, next) {
    const coopId = req.params.coopId
    const studentId = req.params.studentId
    let boolRegistered = false
    try {
      const coops = await Coop.find({
        _id: { $ne: coopId },
        'registers.student': studentId
      })
      for (let i = 0; i < coops.length; i++) {
        for (let j = 0; j < coops[i].registers.length; j++) {
          if (
            coops[i].registers[j].student == studentId &&
            coops[i].registers[j].status < 4
          ) {
            boolRegistered = true
            break
          }
        }
        if (boolRegistered) break
      }
      res.json(boolRegistered)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getCoop (req, res, next) {
    const { id } = req.params
    try {
      const coop = await Coop.findById(id)
        .populate('company')
        .populate('position')
        .populate('interview.location')
        .populate('registers.student')

      res.json(coop)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getHistory (req, res, next) {
    const { id } = req.params
    try {
      let coops = await Coop.find({
        'registers.student': id
      })
        .populate('company')
        .populate('position')
        .populate('interview.location')
        .sort({ _id: -1 })


      for (let i = 0; i < coops.length; i++) {
        let myObj = {}
        for (let j = 0; j < coops[i].registers.length; j++) {
          if (coops[i].registers[j].student._id == id) {
            myObj = { ...coops[i].registers[j] }
            break
          }
        }
        coops[i].registers = [myObj]
      }

      res.json(coops)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getLastRegistering (req, res, next) {
    const { id } = req.params
    try {
      const coops = await Coop.find({
        'registers.student': id
      })
        .populate('company')
        .populate('position')
        .populate('interview.location')
        .sort({ _id: -1 })

      
      let coop = null
      for (let i = 0; i < coops.length; i++) {
        let myObj = null
        for (let j = 0; j < coops[i].registers.length; j++) {
          if (coops[i].registers[j].student._id == id && coops[i].registers[j].status < 4) {
            myObj = { ...coops[i].registers[j] }
            break
          }
        }
        coops[i].registers = [myObj]
        
        if (myObj !== null) {
          coop = coops[i]
          break          
        }
      }

      res.json(coop)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addCoop (req, res, next) {
    let coop = new Coop(req.body)
    coop.company = coop.company._id
    try {
      await coop.save()
      res.json(coop)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateCoop (req, res, next) {
    try {
      const coop = await Coop.updateOne({ _id: req.body._id }, req.body)
      res.json(coop)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteCoop (req, res, next) {
    const { id } = req.params
    try {
      const coop = await Coop.deleteOne({ _id: id })
      res.json(coop)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updatePrivateStatus (req, res, next) {
    try {
      const coop = await Coop.findOne({ _id: req.body._id })
      for (let i = 0; i < coop.registers.length; i++) {
        if (coop.registers[i].student == req.body.student) {
          coop.registers[i].status = req.body.status
          break
        }
      }

      const coopAfter = await Coop.updateOne(
        { _id: req.body._id },
        coop
      )

      res.json(coopAfter)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updatePublicStatus (req, res, next) {
    try {
      const coop = await Coop.findOne({ _id: req.body._id })
      for (let i = 0; i < coop.registers.length; i++) {
        coop.registers[i].status = req.body.status
      }

      const coopAfter = await Coop.updateOne(
        { _id: req.body._id },
        coop
      )

      res.json(coopAfter)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = coopController
