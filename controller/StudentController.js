const Student = require('../models/Student')
const studentController = {
  async getStudents (req, res, next) {
    try {
      const students = await Student.find({})
      res.json(students)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getStudent (req, res, next) {
    const { id } = req.params
    try {
      const student = await Student.findById(id)
      res.json(student)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async hasStudent (req, res, next) {
    const { username } = req.params
    try {
      const student = await Student.findOne({ username: username })
      res.json(student)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addStudent (req, res, next) {
    const student = new Student(req.body)
    try {
      await student.save()
      res.json(student)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateStudent (req, res, next) {
    try {
      const student = await Student.updateOne({ _id: req.body._id }, req.body)
      res.json(student)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteStudent (req, res, next) {
    const { id } = req.params
    try {
      const student = await Student.deleteOne({ _id: id })
      res.json(student)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = studentController
