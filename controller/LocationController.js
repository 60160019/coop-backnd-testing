const Location = require('../models/Location')
const locationController = {
  async getLocations (req, res, next) {
    try {
      const locations = await Location.find({})
      res.json(locations)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getLocation (req, res, next) {
    const { id } = req.params
    try {
      const location = await Location.findById(id)
      res.json(location)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async hasLocation (req, res, next) {
    const { name } = req.params
    try {
      const location = await Location.findOne({ name : name})
      res.json(location)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addLocation (req, res, next) {
    const location = new Location(req.body)
    try {
      await location.save()
      res.json(location)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateLocation (req, res, next) {
    try {
      const location = await Location.updateOne({ _id: req.body._id }, req.body)
      res.json(location)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteLocation (req, res, next) {
    const { id } = req.params
    try {
      const location = await Location.deleteOne({ _id: id })
      res.json(location)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = locationController
