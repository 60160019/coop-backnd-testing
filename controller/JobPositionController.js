const JobPosition = require('../models/JobPosition')
const jobPositionController = {
  async getJobPositions (req, res, next) {
    try {
      const jobPositions = await JobPosition.find({})
      res.json(jobPositions)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getJobPosition (req, res, next) {
    const { id } = req.params
    try {
      const jobPosition = await JobPosition.findById(id)
      res.json(jobPosition)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async hasJobPosition (req, res, next) {
    const { name } = req.params
    try {
      const jobPosition = await JobPosition.findOne({ name: name })
      res.json(jobPosition)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addJobPosition (req, res, next) {
    const jobPosition = new JobPosition(req.body)
    try {
      await jobPosition.save()
      res.json(jobPosition)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateJobPosition (req, res, next) {
    try {
      const jobPosition = await JobPosition.updateOne(
        { _id: req.body._id },
        req.body
      )
      res.json(jobPosition)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteJobPosition (req, res, next) {
    const { id } = req.params
    try {
      const jobPosition = await JobPosition.deleteOne({ _id: id })
      res.json(jobPosition)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = jobPositionController
