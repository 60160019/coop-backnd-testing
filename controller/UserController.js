const User = require('../models/User')
const userController = {
  async getUsers (req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async hasUser (req, res, next) {
    const { username } = req.params
    try {
      const user = await User.findOne({ username: username })
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addUser (req, res, next) {
    const user = new User(req.body)
    try {
      await user.save()
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateUser (req, res, next) {
    try {
      const user = await User.updateOne({ _id: req.body._id }, req.body)
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = userController
