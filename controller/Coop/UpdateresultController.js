// const Student = require('../../models/Student')
const Signuplists = require('../../models/coop/Signup')
const updateResultController = {
  async addResult (req, res) {
    const signup = new Signuplists(req.body)
    try {
      await signup.save()
      res.json(signup)
    } catch (err) {
      res.status(500).json(err)
    }
  },
  async getResult (req, res) {
    try {
      const result = await Signuplists.find({}, { _id: 0 })
      res.json(result)
    } catch (err) {
      res.status(500).json(err)
    }
  }
  // async getStudentlists (req, res) {
  //   try {
  //     const studentlists = await Student.find(
  //       {},
  //       { _id: 0, type: 1, name: 1, surname: 1 }
  //     )
  //     res.json(studentlists)
  //   } catch (err) {
  //     res.stat(500).json(err)
  //   }
  // }
}

module.exports = updateResultController
