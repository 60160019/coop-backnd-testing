const Signup = require('../../models/coop/Signup')
const Student = require('../../models/Student')
const Teacher = require('../../models/Teacher')
const intermshipcontroller = {
  async showResult (req, res) {
    try {
      const getResult = await Signup.find({}, { _id: 0, company: 1, amount: 1 })
      res.json(getResult)
    } catch (err) {
      res.status(500).json(err)
    }
  },
  async showStudents (req, res) {
    try {
      const getStudents = await Student.find(
        {},
        { _id: 0, type: 1, name: 1, surname: 1 }
      )
      res.json(getStudents)
    } catch (err) {
      res.status(500).json(err)
    }
  },
  async showTeacherlists (req, res) {
    try {
      const teacherlist = await Teacher.find({}, { _id: 0, name: 1 })
      res.json(teacherlist)
    } catch (err) {
      res.status(500).json(err)
    }
  },
  async addTeacher (req, res) {
    const { id } = req.params
    const teachername = req.body
    try {
      const addTeacher = await Student.findByIdAndUpdate(id, {
        teacher: teachername
      })
      res.json(addTeacher)
    } catch (err) {
      res.status(500).json(err)
    }
  },
  async showResultdetails (req, res) {
    const { id } = req.params
    try {
      const studentDetails = await Student.findById(id, {
        _id: 0,
        name: 1,
        surname: 1
      })
      res.json(studentDetails)
    } catch (err) {
      res.status(500).json(err)
    }
  }
}

module.exports = intermshipcontroller
