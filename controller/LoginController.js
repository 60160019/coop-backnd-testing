const User = require('../models/User')
const Student = require('../models/Student')
const loginController = {
  async login (req, res, next) {
    try {
      let user = await User.findOne({
        username: req.body.username,
        password: req.body.password
      })
      if (user == null) {
        user = await Student.findOne({
          username: req.body.username,
          password: req.body.password
        })
      }

      if (user != null) {
        user = user.toObject()
        delete user.password
      }
      res.json(user)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = loginController
