const Teacher = require('../models/Teacher')
const teacherController = {
  async getTeachers (req, res, next) {
    try {
      const teachers = await Teacher.find({})
      res.json(teachers)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getTeacher (req, res, next) {
    const { id } = req.params
    try {
      const teacher = await Teacher.findById(id)
      res.json(teacher)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addTeacher (req, res, next) {
    const teacher = new Teacher(req.body)
    try {
      await teacher.save()
      res.json(teacher)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateTeacher (req, res, next) {
    try {
      const teacher = await Teacher.updateOne({ _id: req.body._id }, req.body)
      res.json(teacher)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteTeacher (req, res, next) {
    const { id } = req.params
    try {
      const teacher = await Teacher.deleteOne({ _id: id })
      res.json(teacher)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = teacherController