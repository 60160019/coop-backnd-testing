const Training = require('../models/Training')
const trainingController = {
  async getTrainings (req, res, next) {
    try {
      let trainings
      if (req.params.isPublished === 'isPublished') {
        trainings = await Training.find({ isPublished: true })
          .populate('company')
          .populate('location')
          .populate('participants.student')
          .sort({ _id: -1 })
      } else {
        trainings = await Training.find({})
          .populate('company')
          .populate('location')
          .populate('participants.student')
          .sort({ _id: -1 })
      }

      res.json(trainings)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getTraining (req, res, next) {
    const { id } = req.params
    try {
      const training = await Training.findById(id)
        .populate('company')
        .populate('location')
        .populate('participants.student')

      res.json(training)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getHistory (req, res, next) {
    const { id } = req.params
    try {
      const training = await Training.find({
        isChecked: true,
        'participants.student': id
      })
        .populate('company')
        .populate('location')
        .sort({ _id: -1 })
      res.json(training)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getHoursByType (req, res, next) {
    const id = req.params.id
    const type =
      req.params.type == 1 ? 'ทักษะทางวิชาการ' : 'เตรียมความพร้อมสหกิจศึกษา'
      
    try {
      let obj = {
        type: type,
        defineHours: type === 'ทักษะทางวิชาการ' ? 12 : 30,
        count: 0,
        hours: 0
      }

      const training = await Training.find({
        type: type,
        isChecked: true,
        'participants.student': id
      })

      for (let i = 0; i < training.length; i++) {
        obj.hours += training[i].hours
      }

      obj.count = training.length

      res.json(obj)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addTraining (req, res, next) {
    let training = new Training(req.body)
    training.company = training.company._id
    try {
      await training.save()
      res.json(training)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateTraining (req, res, next) {
    try {
      const training = await Training.updateOne({ _id: req.body._id }, req.body)
      res.json(training)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteTraining (req, res, next) {
    const { id } = req.params
    try {
      const training = await Training.deleteOne({ _id: id })
      res.json(training)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = trainingController
