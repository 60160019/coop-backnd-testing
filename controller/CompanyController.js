const Company = require('../models/Company')
const companyController = {
  async getManyCompany (req, res, next) {
    try {
      const companies = await Company.find({})
      res.json(companies)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async getOneCompany (req, res, next) {
    const { id } = req.params
    try {
      const company = await Company.findById(id)
      res.json(company)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async hasCompany (req, res, next) {
    const { name } = req.params
    try {
      const company = await Company.findOne({ name : name})
      res.json(company)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async addCompany (req, res, next) {
    const company = new Company(req.body)
    try {
      await company.save()
      res.json(company)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async updateCompany (req, res, next) {
    try {
      const company = await Company.updateOne({ _id: req.body._id }, req.body)
      res.json(company)
    } catch (error) {
      res.status(500).send(error)
    }
  },
  async deleteCompany (req, res, next) {
    const { id } = req.params
    try {
      const company = await Company.deleteOne({ _id: id })
      res.json(company)
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = companyController
