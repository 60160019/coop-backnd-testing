var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
var cors = require('cors')
var app = express()
const mongoose = require('mongoose')

mongoose.connect(
  'mongodb+srv://cuser:cuser@coopcluster-zehn9.mongodb.net/coopdb',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => {
  console.log('connect database successful')
})
/** end connect database */

/** require route */
var indexRouter = require('./routes/index')
var loginRouter = require('./routes/login')
var companyRouter = require('./routes/company')
var studentRouter = require('./routes/student')
var teacherRouter = require('./routes/teacher')
var jobpositionRouter = require('./routes/jobposition')
var locationRouter = require('./routes/location')
var userRouter = require('./routes/user')
var trainingRouter = require('./routes/training')
var newsRouter = require('./routes/news')
const coopRouter = require('./routes/coop')
/** end require route */

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/api/login', loginRouter)
app.use('/api/company', companyRouter)
app.use('/api/student', studentRouter)
app.use('/api/teacher', teacherRouter)
app.use('/api/jobposition', jobpositionRouter)
app.use('/api/location', locationRouter)
app.use('/api/user', userRouter)
app.use('/api/training', trainingRouter)
app.use('/api/news', newsRouter)
app.use('/api/coop', coopRouter)

module.exports = app
