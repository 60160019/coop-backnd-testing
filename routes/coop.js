var express = require('express')
var router = express.Router()
const coopController = require('../controller/CoopController')

router.get('/all/:isPublished?', coopController.getCoops)

router.get('/closed/', coopController.getClosedCoops)

router.get('/findOne/:id', coopController.getCoop)

router.get('/history/:id', coopController.getHistory)

router.get('/lastregistering/:id', coopController.getLastRegistering)

router.get(
  '/isRegistered/coopId/:coopId/studentId/:studentId',
  coopController.isRegisteredAnother
)

router.post('/add', coopController.addCoop)

router.put('/edit', coopController.updateCoop)

router.put('/editPrivateStatus', coopController.updatePrivateStatus)

router.put('/editPublicStatus', coopController.updatePublicStatus)

router.delete('/:id', coopController.deleteCoop)

module.exports = router
