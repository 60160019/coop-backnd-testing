var express = require('express')
var router = express.Router()
const locationController = require('../controller/LocationController')

router.get('/', locationController.getLocations)

router.get('/findOne/:id', locationController.getLocation)

router.get('/hasLocation/:name', locationController.hasLocation)

router.post('/add', locationController.addLocation)

router.put('/edit', locationController.updateLocation)

router.delete('/:id', locationController.deleteLocation)

module.exports = router
