var express = require('express')
var router = express.Router()
const trainingController = require('../controller/TrainingController')

router.get('/all/:isPublished?', trainingController.getTrainings)

router.get('/findOne/:id', trainingController.getTraining)

router.get('/history/:id', trainingController.getHistory)

router.get('/hours/id/:id/type/:type', trainingController.getHoursByType)

router.post('/add', trainingController.addTraining)

router.put('/edit', trainingController.updateTraining)

router.delete('/:id', trainingController.deleteTraining)

module.exports = router
