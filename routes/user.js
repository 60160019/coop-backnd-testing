var express = require('express')
var router = express.Router()
const userController = require('../controller/UserController')

router.get('/', userController.getUsers)

router.get('/findOne/:id', userController.getUser)

router.get('/hasUser/:username', userController.hasUser)

router.post('/add', userController.addUser)

router.put('/edit', userController.updateUser)

router.delete('/:id', userController.deleteUser)

module.exports = router
