var express = require('express')
var router = express.Router()
const newsController = require('../controller/NewsController')

router.get('/:isPublished?', newsController.getManyNews)

router.get('/findOne/:id', newsController.getNews)

router.post('/add', newsController.addNews)

router.put('/edit', newsController.updateNews)

router.delete('/:id', newsController.deleteNews)

module.exports = router
