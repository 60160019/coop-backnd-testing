var express = require('express')
var router = express.Router()
const teacherController = require('../controller/TeacherController')

router.get('/', teacherController.getTeachers)

router.get('/:id', teacherController.getTeacher)

router.post('/add', teacherController.addTeacher)

router.put('/edit', teacherController.updateTeacher)

router.delete('/:id', teacherController.deleteTeacher)

module.exports = router
