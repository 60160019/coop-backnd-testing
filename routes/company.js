var express = require('express')
var router = express.Router()
const companyController = require('../controller/CompanyController')

router.get('/', companyController.getManyCompany)

router.get('/findOne/:id', companyController.getOneCompany)

router.get('/hasCompany/:name', companyController.hasCompany)

router.post('/add', companyController.addCompany)

router.put('/edit', companyController.updateCompany)

router.delete('/:id', companyController.deleteCompany)

module.exports = router
