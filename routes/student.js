var express = require('express')
var router = express.Router()
const studentController = require('../controller/StudentController')

router.get('/', studentController.getStudents)

router.get('/findOne/:id', studentController.getStudent)

router.get('/hasStudent/:username', studentController.hasStudent)

router.post('/add', studentController.addStudent)

router.put('/edit', studentController.updateStudent)

router.delete('/:id', studentController.deleteStudent)

module.exports = router
