var express = require('express')
var router = express.Router()
const jobPositionController = require('../controller/JobPositionController')

router.get('/', jobPositionController.getJobPositions)

router.get('/findOne/:id', jobPositionController.getJobPosition)

router.get('/hasJobPosition/:name', jobPositionController.hasJobPosition)

router.post('/add', jobPositionController.addJobPosition)

router.put('/edit', jobPositionController.updateJobPosition)

router.delete('/:id', jobPositionController.deleteJobPosition)

module.exports = router
